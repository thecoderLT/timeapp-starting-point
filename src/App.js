import React, {useState, useEffect, useContext, createContext} from "react";
import Header from "./components/Header";
import Works from "./components/Works";
import WorkById from "./components/WorkById";
import {Alert} from "react-bootstrap";
import Register from "./components/Register";
import Login from "./components/Login";
import Reset from "./components/Reset";
import {AppProvider} from "./context/WorksContext";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import AddWor from "./components/AddWor";

const App = ()=> {
    const[message, setMessage] = useState('');
    const setMessageHandler = (status)=>{
        if(status) {
            setMessage('Paslauga sėkmingai pridėta !')
        }
    }
    useEffect(() => {
        setTimeout(() => {
            setMessage('');
        }, 5000);
    }, [message]);

  return (
    <div className="container">
        <Router>
            <AppProvider>
                <Header/>
                {(message)?
                <Alert variant="success">
                    {message}
                </Alert>:false}
                <Routes>
                    <Route exact path="/" element={<Login/>}/>
                    <Route exact path="/register" element={<Register/>}/>
                    <Route exact path="/reset" element={<Reset/>}/>
                    <Route exact path="/works" element={<Works status={setMessageHandler}/>}/>
                    <Route path="/work/:id" element={<WorkById/>}/>
                    <Route path="/work/update/:id" element={<AddWor/>}/>
                </Routes>
            </AppProvider>
        </Router>
    </div>
  );
}

export default App;
