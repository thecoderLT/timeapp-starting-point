import {SET_WORKS, OPEN_FORM, ADD_WORK_DATA, VALIDATE_WORK, SET_FILTER_CRITERIA, FILTER_WORKS } from "./types";
export const getWorks = (data)=>{
    return{
        type:SET_WORKS,
        payload:data
    }
}

export const openForm = (status)=>{
    return{
        type:OPEN_FORM,
        payload:status
    }
}

export const addWorkData = (data)=>{
    return{
        type:ADD_WORK_DATA,
        payload:data
    }
}

export const  validateWork = (data) =>{
    return{
        type:VALIDATE_WORK,
        payload:data
    }
}

export function setFilterCriteria(data) {
    return {
        type:SET_FILTER_CRITERIA,
        payload:data
    }
}

export function filterWorks(data) {
    return {
        type:FILTER_WORKS,
        payload:data
    }
}