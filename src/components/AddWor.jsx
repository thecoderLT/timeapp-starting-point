import React, {useEffect, useState} from "react";
import {Card, Form,Button, FloatingLabel} from "react-bootstrap";
import Companies from "./companies/Companies";
import Services from "./services/Services";
import * as services from '../services/WorkServices'
import {auth} from "../services/AuthServices";
import {useAuthState} from "react-firebase-hooks/auth";
import {useGlobalContext} from "../context/WorksContext";
import Error from "./Error";
import {useParams, useNavigate} from "react-router-dom";


const AddWork = ()=>{
    const [user,loading, error] = useAuthState(auth)
    const navigate = useNavigate();
    const {id} = useParams();
    const {work, errors, handleWorkData, workValidation, addWorkToFirestore, updateFirestore} = useGlobalContext()

    useEffect(()=>{
        try{
            services.showById(data=>handleWorkData(data), id)
        }catch(error){
            console.log(error)
        }

    },[id])

    const handleChange = (e)=>{
        handleWorkData({[e.target.name]:e.target.value})
    }

    const submitHandler = (event) => {
        event.preventDefault();
        workValidation(work)
        if(Object.keys(errors).length){
            addWorkToFirestore(work)
        }else{
            window.scrollTo(0, 0)
        }

    };

    useEffect(()=>{
        if(user){
            handleWorkData({uid:user.uid})
        }
    },[user])

    return(
        <Card>
            <Card.Header>
                {errors &&
                Object.keys(errors).map((key) => (
                    <Error error={errors[key]}/>
                ))}
            </Card.Header>
            <Card.Header>Pridėkite atliktą darbą</Card.Header>
            <Card.Body>
                <Form onSubmit={submitHandler}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Pasirinkite datą:</Form.Label>
                        <Form.Control type="date" name="date" onChange={handleChange} value={work.date}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <FloatingLabel controlId="floatingSelect" label="Pasirinkite įmonę">
                            <Form.Select aria-label="Floating label select example" name="company" onChange={handleChange} value={work.company}>
                               <Companies/>
                            </Form.Select>
                        </FloatingLabel>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <FloatingLabel controlId="floatingSelect" label="Pasirinkite suteiktą paslaugą">
                            <Form.Select aria-label="Floating label select example" name="service" onChange={handleChange} value={work.service}>
                                <Services/>
                            </Form.Select>
                        </FloatingLabel>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <FloatingLabel controlId="floatingTextarea2" label="Atlikto darbo aprašyma">
                            <Form.Control
                                as="textarea"
                                placeholder="Atlikto darbo aprašymas"
                                style={{ height: '100px' }}
                                name="description"
                                onChange={handleChange}
                                value={work.description}
                            />
                        </FloatingLabel>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Nuo:</Form.Label>
                        <Form.Control type="time" placeholder="N" name="timeFrom" onChange={handleChange} value={work.timeFrom} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Iki:</Form.Label>
                        <Form.Control type="time" placeholder="Enter email" name="timeTo" onChange={handleChange} value={work.timeTo}/>
                    </Form.Group>
                    {(id)?
                        <Button variant="primary" type="button" onClick={
                            ()=>{
                                updateFirestore(id,work)
                            navigate('/works')
                            }
                        }>Atnaujinti</Button>:
                        <Button variant="primary" type="submit">Saugoti</Button>
                    }
                   </Form>
            </Card.Body>
        </Card>
    )
}

export default AddWork