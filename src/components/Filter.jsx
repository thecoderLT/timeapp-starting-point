import {Button, Form} from "react-bootstrap";
import Companies from "./companies/Companies";
import Services from "./services/Services";
import React, {useEffect, useState} from "react";
import {useGlobalContext} from "../context/WorksContext";
const Filter = (props)=>{
    const { filterCriteria, handleFilter } = useGlobalContext()

    const handleChange = (e)=>{

        handleFilter({[e.target.name]:e.target.value})
    }


    const resetFilter = ()=>{
        handleFilter()
    }

    console.log('kriterijai', filterCriteria)

    return (
            <div>
                <Form>
                    <div className="input-group">
                        <div><label>Duomenų filtravimas:</label></div>
                    <div>
                        <Form.Select  name="company" onChange={handleChange} defaultValue="--Pasirinkite įmonę--">
                            <option selected disabled>--Pasirinkite įmonę--</option>
                            <Companies/>
                        </Form.Select>
                    </div>
                    <div>
                        <Form.Select onChange={handleChange} name="service" defaultValue="--Pasirinkite paslaugą--">
                            <option selected disabled>--Pasirinkite paslaugą--</option>
                            <Services/>
                        </Form.Select>
                    </div>
                    <div>
                        { (Object.keys(filterCriteria).length) ?
                            <Button variant="primary" type="reset" onClick={resetFilter}>
                                Valyti filtrą
                            </Button>:null}
                    </div>
                    </div>
                </Form>
            </div>
        );
}

export default Filter