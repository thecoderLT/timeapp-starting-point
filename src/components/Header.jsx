import {Navbar} from "react-bootstrap";
import {Container} from "react-bootstrap";
import {Nav} from "react-bootstrap";
import User from "./User";


const Header = ()=>{
    return(
        <>
            <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="/#">
                        TimeTable app
                    </Navbar.Brand>
                    <Navbar.Collapse className="justify-content-end" style={{marginRight:'50px'}}>
                        <Nav defaultActiveKey="/home" as="ul" variant="pills">
                            <Nav.Item as="li">
                                <Nav.Link href="/companies">Įmonės</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link eventKey="link-1" href="/services">Paslaugos</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link eventKey="link-2">Apie projektą</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Navbar.Collapse>
                    <Navbar.Text style={{color:'#fff'}}>
                       <User/>
                    </Navbar.Text>
                </Container>
            </Navbar>
        </>
    )
}

export default Header