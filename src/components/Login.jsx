import {Form,Button} from "react-bootstrap";
import {useEffect, useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {auth, signInWithEmailAndPassword} from "../services/AuthServices";
import {useAuthState} from "react-firebase-hooks/auth";

const Login = ()=>{
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const[user,error, loading] = useAuthState(auth);
    const navigate = useNavigate();

    const submitHandler = (e)=>{
        e.preventDefault()
        signInWithEmailAndPassword(email, password)
    }

    useEffect(()=>{
        if(loading) return
        if(user) navigate("/works")
    },[user, loading])

    return(
        <>
            <h2 className="mt-3 text-center">Galite prisijungti</h2>
            <Form onSubmit={submitHandler}>
                <Form.Group className="mx-auto col-sm-6">
                    <Form.Control
                    type="email"
                    placeholder="El. pasto adresas"
                    onChange={(e)=>setEmail(e.target.value)}
                    value={email}
                    />
                </Form.Group>
                <Form.Group className="mx-auto col-sm-6">
                    <Form.Control
                        type="password"
                        placeholder="Slaptazodis"
                        onChange={(e)=>setPassword(e.target.value)}
                        value={password}
                    />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Prisijungti
                </Button>
                <div>
                    <ul>
                        <li>Neturite paskyros ?<Link to="/register">Galite prisiregistruoti</Link></li>
                        <li>Pamirsote slaptazodi ?<Link to="/reset">Atkurkite</Link></li>
                    </ul>
                </div>

            </Form>
        </>
    )
}


export default Login