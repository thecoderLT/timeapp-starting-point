import React, {useState, useEffect} from "react";
import {Form, Button} from "react-bootstrap"
import {registerWithEmailAndPassword, auth} from "../services/AuthServices";
import {useNavigate} from "react-router-dom"
import {useAuthState} from "react-firebase-hooks/auth";

const Register = ()=>{
    const [userName, setUserName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [user, error, loading] = useAuthState(auth)

    const navigate = useNavigate();

    useEffect(()=>{
        if(loading) return
        if(user) navigate("/works")

    },[user, loading])

    const submitHandler = (e)=>{
        e.preventDefault()
        if(!userName) alert('Ivesk varda')
        registerWithEmailAndPassword(userName, email, password)
    }


    return(
        <>
            <h2 className="mt-3 text-center">Sukurk paskyrą</h2>
            <Form className="mx-auto col-sm-6" onSubmit={submitHandler}>
                <Form.Group>
                    <Form.Control
                    type="text"
                    placeholder="Iveskite savo varda"
                    value={userName}
                    onChange={(e)=>setUserName(e.target.value)}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Control
                    type="email"
                    placeholder="Iveskite savo el. pasta"
                    value={email}
                    onChange={(e)=>setEmail(e.target.value)}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Control
                    type="password"
                    placeholder="Iveskite savo slaptazodi"
                    value={password}
                    onChange={(e)=>setPassword(e.target.value)}
                    />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Registruokis
                </Button>
            </Form>
        </>
    )

}

export default Register