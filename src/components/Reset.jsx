import {Form, Button} from "react-bootstrap";
import {Link, useNavigate} from "react-router-dom"
import {useAuthState} from "react-firebase-hooks/auth";
import {sendPasswordResetEmail, auth} from "../services/AuthServices";
import {useState, useEffect} from "react";

const Reset = ()=>{
    const [email, setEmail] = useState()
    const navigate = useNavigate();
    const submitHandler = (e)=>{
        e.preventDefault()
        if(!email) alert('Ivesk el. pasta')
        sendPasswordResetEmail(email)
        navigate("/login")
    }
return(
    <>
        <h2 className="mt-3 text-center">Atstatykite slaptažodį</h2>
        <Form className="mx-auto col-sm-6" onSubmit={submitHandler}>
            <Form.Control
            type="email"
            placeholder='El. pašto adresas'
            onChange={(e)=>setEmail(e.target.value)}
            value={email}
            />
            <Button type="submit">Atstatykite</Button>
        </Form>
        <div>
            Neturite paskyros ? <Link to="/register">Registruokis</Link>
        </div>
    </>
)
}

export default Reset