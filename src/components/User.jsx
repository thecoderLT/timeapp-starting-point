import {useEffect, useContext, useState} from "react";
import {Navbar, NavDropdown, NavbarCollapse} from "react-bootstrap";
import {auth, logout} from "../services/AuthServices";
import {useAuthState} from "react-firebase-hooks/auth";
import * as userService from "../services/UserServices";
import {useNavigate} from "react-router-dom";

const User = ()=>{
    const [userData,setUserData] = useState({});
    const [user, loading, error] = useAuthState(auth);
    const navigate = useNavigate();
    useEffect(()=> {
        if (loading) return;
        if (!user) return navigate("/");
        userService.getUserData(user, setUserData)
    },[user,loading])

    return(

        <Navbar.Collapse className="justify-content-end">
            {user &&
            <NavDropdown title={userData.name} id="fff basic-nav-dropdown">
                <NavDropdown.Item>{userData.email}</NavDropdown.Item>
                <NavDropdown.Divider/>
                <NavDropdown.Item onClick={logout}>Atsijungti</NavDropdown.Item>
            </NavDropdown>
            }
        </Navbar.Collapse>

    )

}

export default User