import {Pencil} from 'react-bootstrap-icons';
import {Trash} from 'react-bootstrap-icons';
import {Link} from "react-router-dom";
import diff from "../utilities/timeCalc";
import {useGlobalContext} from "../context/WorksContext";
const Work = (props)=>{
   const {deleteFromFirestore} = useGlobalContext()
    return(
        <tr>
            <td>{props.date}</td>
            <td>{props.company}</td>
            <td>{props.service}</td>
            <td>{props.timeFrom && diff(props.timeFrom,props.timeTo)}</td>
            <td><Link to={`/work/update/${props.id}`}><Pencil/></Link></td>
            <td><a href="#/" onClick={()=>{deleteFromFirestore(props.id)}}><Trash/></a></td>
            <td><Link key={props.id} to={`/work/${props.id}`}>Plačiau...</Link></td>
        </tr>
    )
}

export default Work