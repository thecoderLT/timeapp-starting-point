import {Card, Button} from "react-bootstrap";
import AddWork from "./AddWor";
import Filter from "./Filter";
import WorksTable from "./WorksTable";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../services/AuthServices";
import {useNavigate} from "react-router-dom"
import {useGlobalContext} from "../context/WorksContext";

const Works = ()=>{
    const {works, filtered, isOpen, handleForm,filterCriteria} = useGlobalContext();
    const [user, error, loading] = useAuthState(auth)

    console.log('filtras', filtered)

    return(
        <>
            <Card>
                <Card.Header>{(isOpen)?
                    <Button className="btn btn-danger" onClick={()=>{handleForm(false)}}>Atšaukti</Button>:
                    <Button className="btn btn-primary" onClick={()=>{handleForm(true)}}>Pridėti</Button>}
                </Card.Header>
                <Card.Header>
                    <Filter/>
                </Card.Header>
                <Card.Header>
                    {(isOpen) && <AddWork/>}
                </Card.Header>
                {works && <>
                    <Card.Header><h3>Darbų sąrašas:</h3></Card.Header>
                    <Card.Body>
                        {(Object.keys(filterCriteria).length)?
                            <WorksTable data={filtered}/>:
                            <WorksTable data={works}/>
                        }
                    </Card.Body>
                </>}
            </Card>
        </>
    )
}

export default Works