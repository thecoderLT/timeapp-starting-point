import Work from "./Work";
import React, {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import {Pencil} from 'react-bootstrap-icons';
import * as services from '../services/WorkServices'



const WorksTable = (props)=>{
    const [sort, setSort] = useState('')
    const deleteItemHandler = (id)=>{
    services.deleteWork(id)
    }



    const setSortHandler = ()=>{
        setSort('COMPANY_DESC')
    }



    return(
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>Data</th>
                <th>Klientas<Pencil onClick={setSortHandler}/></th>
                <th>Suteikta paslauga</th>
                <th>Trukmė</th>
                <th>Keisti</th>
                <th>Šalinti</th>
                <th>Plačiau</th>
            </tr>
            </thead>
            <tbody>
            {
                props.data.map((i)=>(
                    <Work id={i.id} date={i.date} company={i.company} service={i.service}
                                            timeFrom={i.timeFrom}  timeTo={i.timeTo}
                    delete={deleteItemHandler}
                    />))

            }
            </tbody>
        </Table>
    )
}


export default WorksTable