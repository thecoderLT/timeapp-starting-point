import React, { useContext, useEffect, useReducer } from 'react'
import { useAuthState } from "react-firebase-hooks/auth";
import * as service from "../services/WorkServices";
import {auth} from "../services/AuthServices";
import {getWorks, openForm, addWorkData, validateWork, filterWorks, setFilterCriteria} from "../actions/WorksActions";
import WorkReducer from "../reducers/WorksReducer";
import workValidation from "../utilities/workValidation";


const initialState = {
    works: [],
    work:{
        date:'',
        company:'',
        service:'',
        description:'',
        timeFrom:'',
        timeTo:''
    },
    errors:[],
    isOpen: false,
    filterCriteria: {},
    filtered: [],
}

const AppContext = React.createContext()

const AppProvider = ({ children }) => {
    const [user, loading, error] = useAuthState(auth);
    const [state, dispatch] = useReducer(WorkReducer, initialState)


    useEffect(()=>{
        if (loading) {
            return;
        }
            try {
                service.getAllWorks((data) => {
                    dispatch(getWorks(data)
                    )
                }, user)

            } catch (error) {
                console.log(error)
            }


    },[user, loading])

    const handleForm  = (status)=>{
        dispatch(openForm(status))
    }

    const handleWorkData = (input) =>{
        dispatch(addWorkData(input))
    }

    const workValidation = (data)=>{
        dispatch(validateWork(data))
    }

    const addWorkToFirestore = (data)=>{
        try{
            service.addWork(data)
        }catch(error){
            console.log(error)
        }
        handleForm(false)
    }

    const updateFirestore  = (id,data)=>{
        try{
            service.updateWork(id,data)
        }catch(error){
            console.log(error)
        }
    }

    const deleteFromFirestore = (id)=>{
        try{
            service.deleteWork(id)
        }catch(error){
            console.log(error)
        }
    }

    const handleFilter = (criteria) => {
        dispatch(setFilterCriteria(criteria))
    }


    useEffect(() => {
        if(state.filterCriteria) {
            dispatch(filterWorks(state.filterCriteria))
        }
    }, [state.filterCriteria])


    return (
        <AppContext.Provider value={{ ...state,handleForm, handleWorkData, workValidation, addWorkToFirestore, updateFirestore, deleteFromFirestore, handleFilter }}>
            {children}
        </AppContext.Provider>
    )
}

export const useGlobalContext = () => {
    return useContext(AppContext)
}

export { AppContext, AppProvider }