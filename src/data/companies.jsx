const companies = [
    {
        code:12345,
        title: "IT profai"
    },
    {
        code:12346,
        title: "React masters"
    },
    {
        code:12347,
        title: "WEB dream world"
    },
    {
        code:12348,
        title: "Testing unit"
    },
    {
        code:12349,
        title: "Bits of byte"
    }
]

export default companies;