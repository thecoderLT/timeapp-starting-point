import {SET_WORKS, OPEN_FORM,ADD_WORK_DATA, VALIDATE_WORK, SET_FILTER_CRITERIA,FILTER_WORKS} from "../actions/types";
import workValidation from "../utilities/workValidation";
import worksFilter from "../utilities/worksFilter";
const WorksReducer = (state,action)=>{
    switch(action.type){
    case SET_WORKS:
            return {
                ...state,
                works: action.payload,
            }
      case OPEN_FORM:
            return {
                ...state,
                isOpen: action.payload,
            }

      case ADD_WORK_DATA:
            return {
                ...state,
                work: {...state.work, ...action.payload}
            }
        case VALIDATE_WORK:
            return {
                ...state,
                errors: workValidation(action.payload)
            }

        case SET_FILTER_CRITERIA:
            return {
                ...state,
                filterCriteria: {...state.filterCriteria, ...action.payload}
            }

        case FILTER_WORKS:
            return{
                ...state,
                filtered: worksFilter(action.payload, [...state.works])
            }

    default:
        return state;
    }
}

export default WorksReducer