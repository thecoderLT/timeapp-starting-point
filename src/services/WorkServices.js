import firebase from "../firebaseConfig";



export const getAllWorks = (onTimesChanged, user)=> firebase
    .firestore()
    .collection('times')
    .where("uid", "==", user?.uid)
    .get()
    .then((snapshot) => {

        const newTimes = (snapshot.docs.length)? snapshot.docs.map((doc) =>(
            {
                id: doc.id,
                ...doc.data()
            })):null
        onTimesChanged(newTimes)

    })

export const addWork = (data,onSucess)=>{
     firebase
         .firestore()
         .collection('times')
         .add(data)
         .then(onSucess())
}

export const showById = (item, id)=>{
    firebase
        .firestore()
        .collection('times')
        .doc(id)
        .get()
        .then((docRef)=>{item(docRef.data())})
}

export const deleteWork = (id)=>{
firebase
    .firestore()
    .collection('times')
    .doc(id)
    .delete()
}

export const updateWork = (id,data)=>{
    firebase
        .firestore()
        .collection('times')
        .doc(id)
        .set(data)
}



