const workValidation = (values) =>{
    const errors = {}
    if(!values.date){
        errors.date = "Datą nurodyti privaloma"
    }
    if(!values.company){
        errors.company = "Kompaniją nurodyti privaloma"
    }
    if(!values.service){
        errors.service = "Suteiktą paslaugą pasirinkti privaloma"
    }
    if(!values.description){
        errors.description = "Susteiktos paslaugos aprašymą nurodyti privaloma"
    }
    if(!values.timeFrom){
        errors.timeFrom = "Laiką nuo nurodyti privaloma"
    }
    if(!values.timeTo){
        errors.timeTo = "Laiką iki nurodyti privaloma"
    }

    return errors
}

export default workValidation