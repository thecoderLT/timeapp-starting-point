const filteredItems = (filterCriteria,data) => {
    const filterKeys = Object.keys(filterCriteria);
    return data.filter(eachObj => {
        return filterKeys.every(eachKey => {
            if (!filterCriteria[eachKey].length) {
                return true; // Jeigu nera reiksmiu filtre, filtras yra ignoruojamas

            }
            return filterCriteria[eachKey].includes(eachObj[eachKey]);
        });
    });
};

export default filteredItems